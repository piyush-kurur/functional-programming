import Data.Char

capitalize :: String -> String
capitalize = map toUpper

main :: IO ()
main = (capitalize <$> getContents) >>= putStr
