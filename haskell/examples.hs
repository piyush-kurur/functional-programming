x :: Int
x = 10

y :: (Int, String)
y = (42 , "Is the answer")

u :: [Integer]
u = 1 : u

fibs :: [Integer]
fibs = 2 : (3 : rest)
  where rest  = zipWith (+) fibs fibsP
        fibsP = tail fibs
        {- x : xs  is that list whose first element is x and the rest of the list is xs -}
        {- zipWith f [a1,a2,a3...] [b1,b2,b3...]  = [f a1 b1 , f a2 b2, .... ] -}


u10 :: [Integer]
u10 = take 10 u
