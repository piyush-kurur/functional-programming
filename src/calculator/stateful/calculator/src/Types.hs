{-# LANGUAGE OverloadedStrings #-}
module Types
       ( Expr(..), eval
       , Stmt(..)
       , Result
       , Env, setVar, getVar, emptyEnv
       ) where

import           Control.Applicative
import qualified Data.Map as M
import           Data.Monoid
import           Data.Text


type Result a = Either Text a
type Env      = M.Map Text Double
data Stmt     = Eval Expr
              | Assign Text Expr
              | EmptyStmt
              deriving Show
data Expr = Constant Double
          | Variable Text
          | Plus    Expr Expr
          | Minus   Expr Expr
          | Mul     Expr Expr
          | Div     Expr Expr
     deriving Show
-- | The empty environment
emptyEnv :: Env
emptyEnv = M.empty

setVar :: Text -> Double -> Env -> Env
setVar = M.insert

getVar :: Text -> Env -> Result Double
getVar v = maybe (Left err) Right . M.lookup v
     where err = v <> " undefined"


eval :: Expr -> Env -> Result Double
eval (Constant  c) = const $ return c
eval (Variable  v) = getVar v
eval (Plus  e1 e2) = apply (+) e1 e2
eval (Minus e1 e2) = apply (-) e1 e2
eval (Mul   e1 e2) = apply (*) e1 e2
eval (Div   e1 e2) = expDivide e1 e2

apply :: (Double -> Double -> Double) -- ^ operator
      -> Expr                         -- ^ expression 1
      -> Expr                         -- ^ expression 2
      -> Env                          -- ^ environment to evaluate
      -> Result Double
apply op e1 e2 env = op <$> eval e1 env
                        <*> eval e2 env


expDivide :: Expr -> Expr -> Env -> Result Double
expDivide e1 e2 env = do v2 <- eval e2 env
                         v1 <- eval e1 env
                         if v2 == 0 then Left "division by zero"
                                    else return (v1/v2)
