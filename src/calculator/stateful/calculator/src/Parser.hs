module Parser
       ( getStmt
       )
       where
import           Control.Applicative((<$>))
import qualified Data.Text.Lazy as L
import qualified Data.Text as T
import           Data.Functor.Identity(Identity)
import           Text.Parsec
import qualified Text.Parsec.Token as Token
import           Text.Parsec.Text.Lazy
import           Text.Parsec.Expr

import Types

-- | Parse a text to the corresponding statement.
getStmt :: L.Text -> Result Stmt
getStmt = either errToText Right . parse stmt ""
        where errToText = Left . T.pack . show


-- | The statement parser
stmt :: Parser Stmt
stmt = do whiteSpace
          s <- emptyStmt <|> try assignment <|> evalExpr
          eof
          return s
       <?> "statement"
  where assignment = do v <- variable
                        equals
                        e <- expr
                        return $ Assign v e

        evalExpr   = Eval <$> expr
        emptyStmt  = do eof
                        return EmptyStmt
        equals     = reservedOp "="

-- | The expression parser
expr :: Parser Expr
expr = buildExpressionParser opTable (term <?> "term") <?> "expression"

-- | The basic expression parser
term :: Parser Expr
term =   Constant <$> constant
     <|> Variable <$> variable
     <|> parens expr


-- | The operator table.
opTable :: OperatorTable L.Text () Identity Expr
opTable = [ [ Infix (binOp "*" Mul) AssocLeft
            , Infix (binOp "/" Div) AssocLeft
            ]
          , [ Infix (binOp "+" Plus ) AssocLeft
            , Infix (binOp "-" Minus) AssocLeft
            ]
          ]
  where binOp f func = do _ <- reservedOp f
                          return func

-- | A constant
constant :: Parser Double
constant =   try float
         <|> integer <?> "constant"
   where integer = fromInteger <$> Token.integer calcTokens
                   <?> "integer"
         float   = Token.float calcTokens <?> "real number"

-- | A variable
variable :: Parser T.Text
variable = T.pack <$> Token.identifier calcTokens <?> "variable"

-- | A reserved operator
reservedOp :: String -> Parser ()
reservedOp = Token.reservedOp calcTokens

-- | Skiping white spaces
whiteSpace = Token.whiteSpace calcTokens

-- | A parentesized parser.
parens :: Parser a -> Parser a
parens = Token.parens calcTokens

-- | The token definition for our expression grammar
calcTokens  :: Token.GenTokenParser L.Text () Identity
calcTokens  = Token.makeTokenParser
            $ Token.LanguageDef { Token.commentStart    = ""
                                , Token.commentEnd      = ""
                                , Token.commentLine     = "#"
                                , Token.nestedComments  = False
                                , Token.identStart      = letter <|> char '_'
                                , Token.identLetter     = alphaNum <|> char '_'
                                , Token.opStart         = oneOf "=+*-/"
                                , Token.opLetter        = oneOf "=+*-/"
                                , Token.reservedNames   = []
                                , Token.reservedOpNames = [ "="
                                                          , "+", "-"
                                                          , "*", "/"
                                                          ]
                                , Token.caseSensitive   = True
                                }
