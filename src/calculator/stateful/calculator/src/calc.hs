{-# LANGUAGE OverloadedStrings #-}
import Control.Applicative
import Control.Monad.State
import Data.Monoid
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy.IO as LIO
import System.IO

import Types
import Parser

-- | The calculator monad
type CalcM a = StateT Env IO a

-- | Perform an IO action. I have given a name for better documentation.
doIO :: IO a -> CalcM a
doIO = lift

-- | Run the calculator monad action
runCalcM :: CalcM a -> IO a
runCalcM calcM = evalStateT calcM emptyEnv

-- Some IO functions.

-- | CalcM version putStrLn
putTextLn :: T.Text -> CalcM ()
putTextLn = doIO . TIO.putStrLn

-- | print error message
printError :: T.Text -> CalcM ()
printError mesg = do putTextLn $ "error: " <> mesg
                     doIO $ hFlush stdout

printValue :: Double -> CalcM ()
printValue = putTextLn . T.pack . show



-- | Prints a prompt and reads a single statement from the user.
getInput :: CalcM (Result Stmt)
getInput = doIO $ getStmt <$> LIO.getLine

main :: IO ()
main = runCalcM calcLoop

-- | Handle the calculator errors.
handleCalcError :: (a -> CalcM ()) -> Result a -> CalcM ()
handleCalcError = either printError

-- The rest of the file is the core of the calculator is here.

-- | This is the read eval print loop of the calculator.
calcLoop :: CalcM ()
calcLoop = do interpret
              c <- doIO $ isEOF
              if c then return ()
                   else calcLoop


-- | Evaluates an expression inside the CalcM.
evalExpr :: Expr -> CalcM (Result Double)
evalExpr = gets . eval
-- evalExpr e = gets $ eval e

-- | Updates the environment in the CalcM
updateEnv :: T.Text -> Double -> CalcM ()
updateEnv v d = modify $ setVar v d


-- | Interpret a single line of user input.
interpret :: CalcM ()
interpret = getInput >>= handleCalcError interpretStmt

-- | Interpret a statement
interpretStmt :: Stmt -> CalcM ()
interpretStmt EmptyStmt    = return ()
interpretStmt (Assign v e) = assign   v e
interpretStmt (Eval e    ) = evalStmt e

-- | handle an assignment statement
assign :: T.Text -> Expr -> CalcM ()
assign v e = evalExpr e >>= handleCalcError (updateEnv v)

-- | handle an evaluation statement
evalStmt :: Expr -> CalcM ()
evalStmt e = evalExpr e >>= handleCalcError printValue
