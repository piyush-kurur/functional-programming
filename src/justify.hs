import qualified Data.List as List

spaces n = List.replicate n ' '
data Justify = R
             | L
             | C

justify :: Justify -> Int -> String -> String
justify j n s = spaces pre ++ s ++ spaces suf
  where p         = n - length s
        (pre,suf) = case j of
                      L -> (0, p)
                      R -> (p, 0)
                      _ -> (p `quot` 2, p - pre)

flushLeft  :: Int ->  String -> String
flushLeft  = justify L

flushRight :: Int -> String -> String
flushRight = justify R

centre  :: Int -> String -> String
centre     = justify C

-- | This is the catching the tail style justification function.
--
justifyList :: Justify -> [String] -> [String]
justifyList j xs = result
  where mper :: Int -> Int -> String -> (Int,String)
        mper jm lm str = (max lm $ length str,  justify j jm str)
        (lmax,result)  = List.mapAccumL (mper lmax) 0 xs
